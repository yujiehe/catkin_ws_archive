cmake_minimum_required(VERSION 2.8.3)
project(stm32_serial)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  message_generation
  roscpp
  rospy
  std_msgs
  serial
)


## Generate messages in the 'msg' folder
add_message_files(FILES
  base_rw.msg
)

## Generate added messages and services with any dependencies listed here
generate_messages(DEPENDENCIES
  serial
  std_msgs
)

###################################
## catkin specific configuration ##
###################################
catkin_package(
  CATKIN_DEPENDS
  message_runtime
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include ${catkin_INCLUDE_DIRS}
# include
  ${catkin_INCLUDE_DIRS}
)

add_executable(com_pub src/com_pub.cpp

)
add_dependencies(com_rw ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(com_rw
  ${catkin_LIBRARIES}
)

add_executable(com_l
  src/com_sub.cpp
)
add_dependencies(com_l ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(com_l
  ${catkin_LIBRARIES}
)

add_executable(serial_rw
  src/serial_rw.cpp
)
add_dependencies(serial_rw ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(serial_rw
  ${catkin_LIBRARIES}
)