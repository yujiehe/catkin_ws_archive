/*********************************
 * 串口节点，订阅IMU话题并发布MOTOR话题
 * 
 * *******************************/

//向着串口发送数据
//读取来自串口的数据
//ROS头文件
#include <ros/ros.h>
//包含自定义msg产生的头文件
#include <msg/base_rw.h>

int main(int argc, char **argv)
{
  //初始化ROS内的节点
  ros::init(argc, argv, "base_rw_publisher");
  //实例化节点
  ros::NodeHandle nh;


  //第二个参数是队列大小，以防我们处理消息的速度不够快，当缓存达到 100 条消息后，再有新的消息到来就将开始丢弃先前接收的消息。
  //订阅来自com口的消息，未完成
  ros::Publisher pub_base_rw = nh.advertise<stm32_serial::base_rw>("base_rw", 10);

  //指定发布消息的频率，这里指100Hz，也即每秒100次，通过 Rate::sleep()来处理睡眠的时间来控制对应的发布频率
  ros::Rate loop_rate(100);

  int count = 0;
  while (ros::ok())
  {
//----------自定义发布类型，应该与msg格式一致---------
    stm32_serial::base_rw output;
    
    output.speed[0] = ;
    output.speed[0] = ;
    output.speed[0] = ;
    output.speed[0] = ;
    
    pub_base_rw.publish(output);
	
    //ros::spin()用于调用所有可触发的回调函数。将进入循环，不会返回，类似于在循环里反复调用ros::spinOnce()。
   //ros::spin(); 
    ros::spinOnce();
    loop_rate.sleep();
    count++;
  }


  return 0;
}