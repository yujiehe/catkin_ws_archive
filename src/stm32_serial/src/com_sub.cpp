//读取来自串口的数据
//ROS头文件
#include <ros/ros.h>
//包含自定义msg产生的头文件
#include <msg/base_rw.h>

void base_rwCallback(const stm32_serial::base_rw msg)
{
    // printf("id %d\n",msg.id);
    // printf("len %d\n",msg.len);
    // printf("data [%d,%d,%d,%d]\n",msg.data[0],msg.data[1],msg.data[2],msg.data[3]);
}

int main(int argc, char **argv)
{
  //初始化ROS内的节点
  ros::init(argc, argv, "base_rw_listener");
  //实例化节点
  ros::NodeHandle nh;
  //要订阅 base_rw 话题上的消息。当有消息发布到这个话题时，ROS 就会调用 xxxCallback() 函数。
  //第二个参数是队列大小，以防我们处理消息的速度不够快，当缓存达到 100 条消息后，再有新的消息到来就将开始丢弃先前接收的消息。
  //订阅来自com口的消息，未完成
  ros::Subscriber sub_base_rw = nh.subscribe("base_rw", 100, base_rwCallback);
  //指定发布消息的频率，这里指100Hz，也即每秒10次，通过 Rate::sleep()来处理睡眠的时间来控制对应的发布频率
  ros::Rate loop_rate(100);

  //ros::spin()用于调用所有可触发的回调函数。将进入循环，不会返回，类似于在循环里反复调用ros::spinOnce()。
  ros::spin(); 
  return 0;
}
