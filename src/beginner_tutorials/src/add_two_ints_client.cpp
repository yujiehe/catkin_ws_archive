#include "ros/ros.h"
#include "beginner_tutorials/AddTwoInts.h"
// 在c++文件声明中既有#include <stdlib.h>也有#include <cstdlib.h>
// 一般更推荐使用C++风格的头文件，即 #include <cstdlib>。
// cstdlib实现了stdlib.h中的所有功能
// 不过是按照C++的方式写的，所以与C++语言可以更好的配合。
//包含了以下函数： 
//1 字符串转换为数字的函数，包括atoi, atof, strtol等。 
//2 随机数函数，包括srand, rand等。 
//3 内存分配释放函数，如malloc,calloc,realloc,free等。 
//4 程序运行控制函数，如exit, abort等。 
//5 系统访问相关函数，如printenv, setenv，system等。 
//6 常用算法函数，如qsort, bsearch, abs,div等。
#include <cstdlib>

// 输入超过3个数异常处理
int main(int argc, char **argv)
{
  ros::init(argc, argv, "add_two_ints_client");
  if (argc != 3)
  {
    ROS_INFO("usage: add_two_ints_client X Y");
    // return 非零 是异常退出
    return 1;
  }

  ros::NodeHandle n;
  // 为add_two_ints service创建一个client。ros::ServiceClient 对象待会用来调用service。
  ros::ServiceClient client = n.serviceClient<beginner_tutorials::AddTwoInts>("add_two_ints");
  // 实例化一个由ROS编译系统自动生成的service类，并给其request成员赋值。
  // 一个service类包含两个成员request和response。同时也包括两个类定义Request和Response。
  beginner_tutorials::AddTwoInts srv;
  srv.request.a = atoll(argv[1]);
  srv.request.b = atoll(argv[2]);

  // 调用service，成功调用返回true
  if (client.call(srv))
  {
    ROS_INFO("Sum: %ld", (long int)srv.response.sum);
  }
  else
  {
    ROS_ERROR("Failed to call service add_two_ints");
    return 1;
  }

  return 0;
}