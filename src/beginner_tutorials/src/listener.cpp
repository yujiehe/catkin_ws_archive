#include "ros/ros.h"
#include "std_msgs/String.h"

/**
初始化ROS系统
订阅 chatter 话题

进入自循环，等待消息的到达
当消息到达，调用 chatterCallback() 函数
*/

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
// 这是一个回调函数，当接收到 chatter 话题的时候就会被调用。
// 消息是以 boost shared_ptr 指针的形式传输
// 这就意味着你可以存储它而又不需要复制数据。
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
  // ROS话题消息格式("I heard: [%d] [%d]", msg->A, msg->B);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  // NodeHandle::subscribe() 返回 ros::Subscriber 对象,你必须让它处于活动状态直到你不再想订阅该消息。
  // 当这个对象销毁时，它将自动退订 chatter 话题的消息。
  ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);
  
  ros::spin();

  return 0;
}