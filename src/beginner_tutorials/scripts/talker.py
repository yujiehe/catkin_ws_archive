#!/usr/bin/env python
# the first import for python in ROS

import rospy
# reuse the std_msgs/String message type
# (a simple string container) for publishing.
from std_msgs.msg import String

# define a function talker():
def talker():
    # node is publishing to the **chatter** topic using the message type String. 
    # String here is actually the class std_msgs.msg.String. 
    # The queue_size argument is 10
    pub = rospy.Publisher('chatter', String, queue_size=10)
    # rospy has this information before starting communicating with the ROS Master.
    # In this case, your node will take on the name **talker**. 
    #http://wiki.ros.org/rospy/Overview/Initialization%20and%20Shutdown#Initializing_your_ROS_Node
    rospy.init_node('talker', anonymous=True)


    # the loop 10 times per second
    rate = rospy.Rate(10) # 10hz

    while not rospy.is_shutdown():
        hello_str = "hello world %s" % rospy.get_time()
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass